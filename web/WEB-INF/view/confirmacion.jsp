<%--
    Document   : confirmacion
    Created on : Nov 20, 2014, 12:20:43 AM
    Author     : tavo
--%>

<div style="background:  azure">


    <p >
        <strong>Su orden esta lista para ser entregada en una semana</strong>
        <br><br>
        conserve el numero de confirmacion de orden:
        <strong>${regOrden.numConfirmacion}</strong>
        <br>
    </p>


    <table class="tablex"   >
        <tr  >
            <th colspan="4">Contenido de la orden</th>
        </tr>

        <tr  >
            <td>Titulo</td>
            <td>Precio unitario</td>
            <td>Cantidad</td>
            <td>Precio</td>
        </tr>

        <c:forEach var="ordenedLibro" items="${ordenedLibros}" varStatus="iter">

            <tr>
                <td>${libros[iter.index].titulo}</td>
                <td  >
                    $ ${libros[iter.index].precio}
                </td>
                <td  >
                    ${ordenedLibro.cantidad}
                </td>
                <td  >
                    $ ${libros[iter.index].precio * ordenedLibro.cantidad}
                </td>
            </tr>

        </c:forEach>

        <tr  ><td colspan="4" style="padding: 0 20px"><hr></td></tr>

        <tr  >
            <td colspan="3" id="cargoDeEnvioCellLeft"><strong>gastos de envio:</strong></td>
            <td >$ ${initParam.cargoDeEnvio}</td>
        </tr>

        <tr  >
            <td  colspan="3"  id="totalCellLeft"><strong>total:</strong></td>
            <td >$ ${regOrden.total}</td>
        </tr>

        <tr  ><td colspan="4" style="padding: 0 20px"><hr></td></tr>

        <tr  >
            <td colspan="4" ><strong>fecha del proceso:</strong>
                ${regOrden.fechaCreacion}
            </td>
        </tr>
    </table>

    <table class="tablex"   >
        <tr  >
            <th colspan="4">datos del pedido</th>
        </tr>

        <tr>
            <td colspan="4"  >
                <b>Nombre: </b> ${cliente.nombre}
                <br>
                <b>Direccion: </b> ${cliente.direccion}
                <br>
                <b>C.P: </b> ${cliente.cp}
                <br>
                <hr>
                <strong>email:</strong> ${cliente.email}
                <br>
                <strong>telefono:</strong> ${cliente.telefono}
            </td>
        </tr>
    </table>

</div>