<%--
    Document   : categoria
    Created on : Nov 20, 2014, 12:20:23 AM
    Author     : tavo
--%>
<div style="background:  azure">
    <br/><br/>

    <table align="center">

        <c:forEach var="categoria" items="${categorias}">
            <td width="200px" align="center" >
                <a href="<c:url value='categoria?${categoria.id}'/>">
                    <span  ></span>
                    <span  >${categoria.nombre}</span>
                    <br/>

                    <img    width="100" src="${initParam.categoriasImg}${categoria.nombreImg}.gif"
                            alt="${categoria.nombreImg}"  >
                </a>
                <br/>
            </td>
        </c:forEach>

        <tr><td>
                <div align="center">
                    <br/><br/>    
                    <c:forEach var="categoria" items="${categorias}">

                        <c:choose>
                            <c:when test="${categoria.nombre == categoSel.nombre}">
                                <div   id="categoSel">
                                    <span  >
                                        -${categoria.nombre}
                                    </span>
                                </div>
                            </c:when>

                        </c:choose>

                    </c:forEach>
                    <br/><br/>
                </div>
            </td></tr>



        <c:forEach var="libro" items="${categoriaLibros}" varStatus="iter">

            <tr>

                <td>
                    <img src="${initParam.librosImg}${libro.nombreImg}.jpg"
                         alt="${libro.nombreImg}">
                </td>

                <td>
                    ${libro.titulo}
                    <br>
                    <span  >${libro.descripcion}</span>
                    <br>
                    <span  >Autor: ${libro.autor}</span>
                </td>

                <td>$ ${libro.precio}</td>

                <td>
                    <form action="<c:url value='agregaACarro'/>" method="post">
                        <input type="hidden"
                               name="libroId"
                               value="${libro.id}">
                        <input type="submit"
                               name="submit"
                               value="a�adir a carrito">
                    </form>
                </td>
            </tr>

        </c:forEach>


    </table>

</div>