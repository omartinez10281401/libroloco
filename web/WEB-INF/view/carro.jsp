<%--
    Document   : carro
    Created on : Nov 20, 2014, 12:20:12 AM
    Author     : tavo
--%>

<div style="background:  azure">
    <br/><br/>

        <c:choose>
            <c:when test="${carro.numeroDeItems == 0}">
                <p>Tu carrito esta vacio</p>
            </c:when>

        </c:choose>

        <div align="center">
            <c:if test="${!empty carro && carro.numeroDeItems != 0}">

                <c:url var="url" value="vercarro">
                    <c:param name="clear" value="true"/>
                </c:url>

                <a href="${url}"  >Limpiar Carrito</a>
            </c:if>

            <c:set var="value">
                <c:choose>
                    <c:when test="${!empty categoSel}">
                        categoria
                    </c:when>
                    <c:otherwise>
                        index.jsp
                    </c:otherwise>
                </c:choose>
            </c:set>

            <c:url var="url" value="${value}"/>
            <a href="${url}"  >Continuar comprando</a>



        </div>
        <br/><br/><br/><br/>
        <c:if test="${!empty carro && carro.numeroDeItems != 0}">



            <table class="tablex"  align="center">

                <tr  >
                    <th>Libroo</th>
                    <th>Nombre</th>
                    <th>Precio</th>
                    <th>Cantidad</th>
                </tr>

                <c:forEach var="carroItem" items="${carro.items}" varStatus="iter">

                    <c:set var="libro" value="${carroItem.libro}"/>

                    <tr  >
                        <td>
                            <img src="${initParam.librosImg}${libro.nombreImg}.jpg"
                                 alt="${libro.nombreImg}">
                        </td>

                        <td>${libro.titulo}</td>

                        <td>
                            $ ${carroItem.total}
                            <br>
                            <span  >($ ${libro.precio} / unidad )</span>
                        </td>

                        <td>
                            
                            
                            <script src="js/jquery-1.4.2.js" type="text/javascript"></script>
                            <script src="js/jquery.validate.js" type="text/javascript"></script>        
                            <script type="text/javascript">
                        $().ready(function(){
                            $("#cars").validate({
                                rules:{
                                    cantidad:{required:true, number:true}
                                },
                                messages:{
                                    cantidad:{required:"campo requerido",number:"Solo numeros"}                                    
                                }
            
            
                            });
                        });
                            </script>                             
                            <form action="<c:url value='actualizaCarro'/>" method="post" id="cars">
                                <input type="hidden"
                                       name="libroId"
                                       value="${libro.id}">
                                <input type="text"
                                       maxlength="2"
                                       size="2"
                                       value="${carroItem.cantidad}"
                                       name="cantidad"
                                       id="cantidad"
                                       style="margin:5px">
                                <input type="submit"
                                       name="submit"
                                       value="Actualizar">
                            </form>
                        </td>
                    </tr>
                    
                    

                </c:forEach>
                <td  align="right">
                    <h4 id="subtotal">SubTotal: $ ${carro.subtotal}
                        <c:if test="${!empty carro && carro.numeroDeItems != 0}">
                            <a href="<c:url value='pago'/>"  >Proceder a Pagar</a>
                        </c:if></h4>
                </td>
            </table>

        </c:if>

</div>