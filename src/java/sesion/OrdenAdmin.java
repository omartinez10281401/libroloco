/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sesion;

import carro.Carro;
import carro.CarroItem;
import entidad.Cliente;
import entidad.Orden;
import entidad.Ordenlibro;
import entidad.OrdenlibroPK;
import entidad.Libro;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tavo
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class OrdenAdmin {

    @PersistenceContext(unitName = "LibroLocoPU")
    private EntityManager em;
    @Resource
    private SessionContext context;
    @EJB
    private LibroFacade libroFacade;
    @EJB
    private OrdenFacade clienteOrdenFacade;
    @EJB
    private OrdenlibroFacade ordenLibroFacade;

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public int placeOrden(String nombre, String email, String telefono, String direccion, String cp, String tarjNumero, Carro carro) {

        try {
            Cliente cliente = agregaCliente(nombre, email, telefono, direccion, cp, tarjNumero);
            Orden orden = agregaOrden(cliente, carro);
            agregaItemsAOrden(orden, carro);
            return orden.getId();
        } catch (Exception e) {
            e.printStackTrace();
            context.setRollbackOnly();
            return 0;
        }
    }

    private Cliente agregaCliente(String nombre, String email, String telefono, String direccion, String cp, String tarjNumero) {
        Cliente cliente = new Cliente();

        System.out.println(nombre);
        System.out.println(email);
        System.out.println(direccion);
        System.out.println(cp);
        System.out.println(tarjNumero);

        cliente.setNombre(nombre);
        cliente.setEmail(email);
        cliente.setTelefono(telefono);
        cliente.setDireccion(direccion);
        cliente.setCp(cp);
        cliente.setTarjNumero(tarjNumero);

        em.persist(cliente);
        return cliente;
    }

    private Orden agregaOrden(Cliente cliente, Carro carro) {

        System.out.println("elerror en agregaOrden");
        Orden orden = new Orden();
        orden.setClienteId(cliente);
        orden.setTotal(BigDecimal.valueOf(carro.getTotal()));

        java.util.Date dt = new java.util.Date();


        Random random = new Random();
        int i = random.nextInt(999999999);
        orden.setFechaCreacion(dt);
        orden.setNumConfirmacion(i);

        em.persist(orden);
        return orden;
    }

    private void agregaItemsAOrden(Orden orden, Carro carro) {
        System.out.println("elerror en agregaItemsAOrden");
        em.flush();

        List<CarroItem> items = carro.getItems();

        for (CarroItem scItem : items) {

            int libroId = scItem.getLibro().getId();

            OrdenlibroPK ordenedLibroPK = new OrdenlibroPK();
            ordenedLibroPK.setOrdenId(orden.getId());
            ordenedLibroPK.setLibroId(libroId);

 
            Ordenlibro ordenItem = new Ordenlibro(ordenedLibroPK);
            ordenItem.setCantidad(scItem.getCantidad());

            em.persist(ordenItem);
        }
    }

    public Map getOrdenDetalles(int ordenId) {

        Map ordenMap = new HashMap();

        Orden orden = clienteOrdenFacade.find(ordenId);

        Cliente cliente = orden.getClienteId();

        List<Ordenlibro> Libros = ordenLibroFacade.findByOrdenId(ordenId);

        List<Libro> libros = new ArrayList<>();

        for (Ordenlibro op : Libros) {

            Libro p = (Libro) libroFacade.find(op.getOrdenlibroPK().getLibroId());
            libros.add(p);
        }

        ordenMap.put("regOrden", orden);
        ordenMap.put("cliente", cliente);
        ordenMap.put("ordenedLibros", Libros);
        ordenMap.put("libros", libros);

        return ordenMap;
    }

}
