/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author tavo
 */
@Entity
@Table(name = "ordenlibro")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ordenlibro.findAll", query = "SELECT o FROM Ordenlibro o"),
    @NamedQuery(name = "Ordenlibro.findByOrdenId", query = "SELECT o FROM Ordenlibro o WHERE o.ordenlibroPK.ordenId = :ordenId"),
    @NamedQuery(name = "Ordenlibro.findByLibroId", query = "SELECT o FROM Ordenlibro o WHERE o.ordenlibroPK.libroId = :libroId"),
    @NamedQuery(name = "Ordenlibro.findByCantidad", query = "SELECT o FROM Ordenlibro o WHERE o.cantidad = :cantidad")})
public class Ordenlibro implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected OrdenlibroPK ordenlibroPK;
    @Column(name = "cantidad")
    private Short cantidad;
    @JoinColumn(name = "orden_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Orden orden;
    @JoinColumn(name = "libro_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Libro libro;

    public Ordenlibro() {
    }

    public Ordenlibro(OrdenlibroPK ordenlibroPK) {
        this.ordenlibroPK = ordenlibroPK;
    }

    public Ordenlibro(int ordenId, int libroId) {
        this.ordenlibroPK = new OrdenlibroPK(ordenId, libroId);
    }

    public OrdenlibroPK getOrdenlibroPK() {
        return ordenlibroPK;
    }

    public void setOrdenlibroPK(OrdenlibroPK ordenlibroPK) {
        this.ordenlibroPK = ordenlibroPK;
    }

    public Short getCantidad() {
        return cantidad;
    }

    public void setCantidad(Short cantidad) {
        this.cantidad = cantidad;
    }

    public Orden getOrden() {
        return orden;
    }

    public void setOrden(Orden orden) {
        this.orden = orden;
    }

    public Libro getLibro() {
        return libro;
    }

    public void setLibro(Libro libro) {
        this.libro = libro;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ordenlibroPK != null ? ordenlibroPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ordenlibro)) {
            return false;
        }
        Ordenlibro other = (Ordenlibro) object;
        if ((this.ordenlibroPK == null && other.ordenlibroPK != null) || (this.ordenlibroPK != null && !this.ordenlibroPK.equals(other.ordenlibroPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.Ordenlibro[ ordenlibroPK=" + ordenlibroPK + " ]";
    }
    
}
